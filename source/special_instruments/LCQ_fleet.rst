LCQ fleet instrument - Mercator (MS, MS\ :sup:`n`)
==================================================

The direct injection instrument of the Boltje group.

How to operate the machine
--------------------------
Use only for methanol-compatible samples. Follow general notes from
:ref:`sample_preparation_guide`. Follow **operation procedure**

Booking system
--------------
No booking system at the moment. If the instrument gets busy, booking system
will be made on request.

How to gain access
------------------
Contact Evy or Daan.

Operation procedure
-------------------
* Insert your samples (blank + samples).
* Add your samples into the queue (blank + n samples + blank).
* Check if the solvents are there (>300 ml).
* Submit your measurement.
* Wait till you see that the machine is running.
* After finishing the queue, remove the samples.
* Check that the pressure is okay and that the end blank is clean.

When something goes wrong
-------------------------
Contact Evy or Daan.
