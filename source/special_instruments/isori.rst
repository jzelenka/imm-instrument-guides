ISORI ion spectroscopy instrument - Wing4 (MS-UV/VIS/IR)
========================================================

This is just brief operation guide, if the guide differs from Aleks
recommendation, the guide is wrong.

How to operate the machine
--------------------------
* pressure in the He trap must be kept below 4e\ :sup:`-7` mbar (channel #4
  TRAP on the pressure meter)
* Q-Delay of the IR laser for OPO-B should be slowly decreased from 400 μs to
  290 μs before operation.

Follow Aleks advice. In future, guideline will be made

Booking system
--------------
private booking system - the same which is for office/labspace

Switching off the instrument
----------------------------
* Set He-piezo to continuous (by this you effectively turn of He pulses).
* Set trap RF to 0.
* Turn off trap RF generator.
* Turn off the laser shutter.
* Disable all pulses.
* Switch ISORI to Q1 MS mode and set m/z to 10.
* Switch from "Active low" mode to "None".
* Set accumulation to 2000 and turn accumulation on.
* Execute "delayed_heat.bat" file at the desktop.
* Turn off laser shutter and pulsing.
* Set laser Q-delay to 400.
* Turn the laser key to off.
* Switch off TSQ multiplier.
* Clean the ESI source and cap it.
* Switch off the ESI source N2 inlet (big plastic knob in the inlet line above
  your head).
* Check the pressures to verify that He pulses are off and that the source is capped.
* Clean the tables around ISORI.

Network storage
---------------
Windows XP PC. No network storage connected.

Retrieving data
---------------
* Turn off the Eclipse.
* Wait for the Eclipse to fully close.
* Insert your USB stick and copy your data.
* Safely remove your USB stick.
* Reboot the PC.
* Turn on the Eclipse.
* Check that Eclipse started normally.

When something goes wrong
-------------------------
Contact Aleks! If Aleks is not around, check with Guilherme or Jaya

Questions/Misc
--------------
If something is wrong in this guide feel free to contact me.

