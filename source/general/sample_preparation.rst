.. _sample_preparation_guide:

Sample preparation (MS, LC-MS)
==============================

General recommendations for sample preparation for MS and LC-MS analyses.

Samples
-------
All samples must be soluble in the mobile phase used in the (LC-)MS system. If
insoluble sample is injected, it will precipitate-out and get the system stuck.

Solvents
--------
In all cases, use solvents compatible with the mobile phase used in the
(LC-)-MS system. That means:

* For ISQ in Wing4, use either methanol, or water-methanol mixtures. If you are
  using water as a mobile phase, dissolve your sample in water.
* For ISQ in Mercator and LCQ-Fleet, use well-diluted water-acetonitrile
  mixtures or methanol. Samples rich in acetonitrile or methanol can cause bad
  separation and clogging of the instrument in some cases.

Concentration recommendation
----------------------------
Generally, it is good to start with a 10μM solution of your sample. The optimal
sample concentration depends on a sample ability to form ions (ionizability).
Please refer to your MS tutor for concentration recommendations in your
specific case.

For the LC-MS system located in wing 1 (:ref:`isq_wing_1`) you can use slightly
higher starting concentrations around 50μM at your own risk. This difference is
because the sample gets a bit diluted on the column.

Good sample concentration is a concentration at which you see your analyte and
at which the sample is not polluting the system at the same time. Avoid too
concentrated samples both for ISQ and for Accurate Mass sample submission.

Pollutants
----------
Small capillaries and valves are used both in LC and in MS. Any small particles
could stuck those parts and thus leads to instrument down-times. In addition,
some compound notoriously negatively impairs mass spectra and causes
suppression of your desired ion signal.

List of known pollutants
^^^^^^^^^^^^^^^^^^^^^^^^
* polar aprotic solvents (DMF, HMPA..)
* buffers (TEAB ..) and concentrated ionic/easily ionizable compounds (TEA,
  DIPEA..)
* electrolytes (TBAF, TBAPF6, ...)
* molecular sieves, microcrystalline compounds
* low-grade solvents
* concentrated samples
* plastics, plasticizers
* triphenylphosphine-oxide

Gray-zone compounds
^^^^^^^^^^^^^^^^^^^
These compounds lead to ion-suppression and thus to signal impairment.
However, they can be washed-out relatively easily when they are used in low
concentrations. Their use is discouraged, but still allowed for special cases.
Take special care to check that the instrument is free of those
compounds once you're finished.

* DMSO:

  Although it belongs to polar aprotic solvents, it can be washed-out
  relatively easily. Use it in low concentrations\ [#dmsoconc]_ and always
  afterwards wash the system by blank runs up to the point where the signal of
  DMSO is absent (dimethyl sulfoxide, *m/z* 157 - 2x DMSO+H\ :sup:`+`, *m/z*
  169 - 2x DMSO-\ *d6*\ +H\ :sup:`+`).

* TFA (trifluoroacetic acid):

  Commonly used as an additive for liquid chromatography (0.025-0.1%). It tends
  to form ion-pairs with cations in the gas phase.\ [#thermsup]_ Can be partly
  countered by additives.\ [#tfacounter]_ It can be washed out easily.

.. [#dmsoconc] 0.2% DMSO has been tested and one blank run after 7 samples is
   enough to wash-out the contamination.

.. [#thermsup] Widely recognized as a pollutant in the literature. To
   illustrate its effect see :download:`this
   slide<sampleprep_files/TFA_suppresion.pdf>` form Thermo :download:`Mass
   spectrometry Simplified<sampleprep_files/LCQ_ion_max.pdf>` presentation. For
   literature, refer to: C. G. Huber, A. Premstaller, *J. Chromatogr. A*
   **1999**, *849*, 161–173; DOI: `10.1016/S0021-9673(99)00532-4
   <https://doi.org/10.1016/S0021-9673(99)00532-4>`_

.. [#tfacounter] Its effect can be mitigated for instance by addition of acid
   vapours into the ESI head: J. Chen, Z. Liu, F. Wang, J. Mao, Y. Zhou, J.
   Liu, H. Zou, Y. Zhang, *Chem. Commun.* **2015**, *51*, 14758–14760; DOI:
   `10.1039/C5CC06072A <https://doi.org/10.1039/C5CC06072A>`_


Methods for preparing a sample
------------------------------
Methods for preparing samples differ from group to group. Please refer to your
MS tutor for sample preparation method in your specific case.

