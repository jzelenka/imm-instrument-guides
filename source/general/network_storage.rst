.. _network_storage:

Network storage
===============
For MS spectra there is a network storage located on:
**\\\\msspectra-srv.science.ru.nl\\msspectra\\**

Everyone can login using the science (CNCZ) credentials. The U#/S# credentials
wont work. For more information, refer to `CNCZ wiki entry
<https://cncz.science.ru.nl/en/howto/mount-a-network-share/>`_

Uploading to the network storage is generally manual (copy/export either .raw,
or .pdf and move it to appropriate network storage folder).

The network storage serves just as a transfer station. Old data will be
periodically deleted from this storage. It should substitute printing and USB
sticks. It is not intended as a final research data storage place for your MS
spectra!!

FAQ
---
* **I cannot connect. What should I do?**
  Quite often, there is an issue stemming from faulty windows credentials.
  There is a workaround through flushing credentials manager. If you get stuck
  on this, please contact me.
