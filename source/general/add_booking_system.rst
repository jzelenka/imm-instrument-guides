.. _add_booking:

Addition to the booking system (MS facilities)
----------------------------------------------
It is preferred that additions to the booking system are requested by the MS
tutor. Addition requests by students are accepted.

If you are already registered in the booking system, I need your:

* S/U number
* Name
* Group name
* MS Tutor name (only if request is done by the student)

If you are not yet registered in the booking system, please register at https://bookings.science.ru.nl/ , I will then activate your account and grant you the access to the MS booking.

Once you've received proper training, just send the request to me.\ [#jan]_

.. [#jan] Jan Zelenka. I prefer using the @science mail -
   j.zelenka@science.ru.nl
