Chromeleon  sample evaluation guide (ISQ)
=========================================

Quick evaluation guide


* **Open the sample** of interest.

  .. image:: integration_guide_images/opening_measurement.png
     :class: gif

  .. image:: integration_guide_images/opening_measurement.gif
     :height: 0

  *opening the sample (double left click)*


* **Select peaks** which you want to integrate.

  .. image:: integration_guide_images/multiple_peaks.png
     :class: gif

  .. image:: integration_guide_images/multiple_peaks.gif
     :height: 0

  *peak selection (left click onto line and drag)*

* You can see an **M/Z spectrum** of the scan which took place at the maximum
  of the peak by clicking on an integrated peak.

  .. image:: integration_guide_images/show_spectra.png
     :class: gif

  .. image:: integration_guide_images/show_spectra.gif
     :height: 0

  *show m/z spectrum (left click into the peak)*



|gifplayer|
