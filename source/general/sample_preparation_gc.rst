.. _sample_preparation_gc_guide:

Sample preparation (GC-FID, GC-MS)
==================================

General recommendations for sample preparation for GC and GC-MS analyses.

Samples
-------
All samples must be reasonable volatile. Otherwise they will just pollute the
system and give no outcome.

Solvents
--------
In all cases, use solvents compatible with the GC column used in the system.\
[#gc_column]_ Choose solvents which has low vapor volume and low heat of
evaporation. Otherwise, you can obtain split peaks, or unusual peak shape. Stay
away of acids and bases (even as a co-solvents). They damage the column through
desilylation quite fast. If you use protic solvents, analytes can get bound
through hydrogen bonds impairing the analysis.

If you can, use:
""""""""""""""""
* *Hexane, heptane, pentane* - good evaporation, low vapor volume.
* *Dichloromethane* - **only for CG-MS**, good evaporation, reasonable vapor volume.
* *Diethyl ether* - good evaporation, low vapor volume.
* *Dichloromethane:Methanol mixture* - **only for GC-MS**, good evaporation when low amounts of methanol are used.
* *Dichloromethane:Isopropyl alcohol mixture* - **Only for GC-MS**, good evaporation when low amounts of isopropanol are used.

Try to avoid:
"""""""""""""
* *Isopropyl alcohol* - high heat of evaporation, viscous - unreliable splitting.
* *Methanol* - high heat of evaporation, high vapor volume.

Do not use:
"""""""""""
* *Acids* - bleeds the column, even as a co-solvents
* *Bases* - bleeds the column, even as a co-solvents
* *Water* - very high heat of evaporation, very high vapor volume
* *halogenated compounds* - **applies only for GC-FID** - they damage the FID detector.

Concentration recommendation
----------------------------
Generally, it is good to start with 1 mg/ml of sample 1 μl injection and split
50. Peak shape determines how you should optimize the splitting ratio. Column
capacity is roughly 5-700 ng/injection/component.

.. [#gc_column] AB-5MS (Abel Bonded) - 30m lenght, 0.25 mm I.D., 0.25 μm film.
   5% diphenyl- 95% dimethyl- siloxane.

