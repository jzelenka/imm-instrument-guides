Accurate-Mass sample submission guide
=====================================

Notes (FAQ)
-----------
* In the submission form show full spectrum, not only the subsection.
* Check your isotopic pattern briefly prior to submission.
* Do not submit too concentrated samples.
* Use a new sample for the Accurate-Mass submission to prevent contamination.
* Use separate form for every submitted sample.
* Do not forget to both send submission form to Jan\ [#jan]_ and put the
  printed version onto the place.

Sample preparation
------------------
Please refer to the :ref:`sample_preparation_guide`.

Sample Concentration
--------------------
In general, follow the recommendations in the submission procedure below. With
first few samples its hard to get the concentration right as every group of
compounds ionizes differently. Best is to start with rather diluted solutions
and then adjust based on the feedback provided with the results.

Submission procedure
--------------------
* If you're submitting for the first time, please say Hi to Jan (HG 03.409), to
  get to know each other a bit. ;)
* Check that your sample is visible in mass spectrum (:ref:`isq_wing_4`,
  LCQ-Fleet in no-column mode, LCQ-Max)
* Check that your isotopic pattern fits.\ [#calc_webpage]_
* Prepare new ~10µM sample of your analyte preferably in MeOH (MeCN, or
  milli-Q) free of additives. Samples of easy-to-ionize compounds (for example
  amines) should be even more diluted.
* Consult with Jan\ [#jan]_ before submitting the sample if you need to use
  additives or if special measures should be taken (for instance if you need to
  perform analysis completely in the aprotic solvent - MeCN, or want to see
  anions).
* Fill in the submission form:
  :download:`xlsx <hrms_files/masssubmissionform.xlsx>`,
  :download:`pdf <hrms_files/masssubmissionform.pdf>`.
  Examples: :download:`example ISQ <hrms_files/AccMassSubmitISQ.pdf>`,
  :download:`example Fleet <hrms_files/AccMassSubmitFleet.pdf>`.
* Submit the samples by placing them into the fridge located in HG03.411 next
  to the balances.
* Mail a filled submission form to j.zelenka@science.ru.nl and put the printed
  version in the place for sample submissions.  
* Once the sample analysis will be done, you will see an e-mail in the inbox
  and the analysis will be in the place for sample submissions.

.. figure:: hrms_images/fridge_closed.jpg
   :height: 200

   *Fridge - location.*


.. figure:: hrms_images/fridge_open.jpg
   :height: 200

   *Fridge - places for the Sample Submission.*

.. figure:: hrms_images/form_submission.jpg
   :height: 200

   *Place for submission forms.*

Few notes about HRMS results
----------------------------
As accurate mass spectrometry measurements are used for compound
identification, It is crucial to correctly interpret the results and be aware
of the limitations of the method. One of the limitations is lack of the
information about purity - having a clean MS spectrum does not mean that your
compound is pure. At the same time, you can have a very pure compound and you
can obtain very crowded MS spectrum with only small peak (or no peak at all) of
your target molecule. The second limitation is that the method is not
conclusive, especially at higher masses. To take an example from ACS author
guidelines - if we take nominal parent m/z of 118, there are no candidate
formulae within 34 ppm of each other. When the ion is of m/z 750.4 and the
formulae are in the range, there are 626 candidate formulae that are possible
within 5 ppm. That means that at m/z 750, an error (and precision) of 0.018 ppm
would be required to eliminate all extraneous possibilities.\ [#acs_jams]_

The abovementioned example clearly shows that higher the mass is, the
more HRMS measurement serves as a guidance that the compound *can be* the
target molecule and less that the compound *is* the target molecule. Because of
that, some journals does not have any strict limit how much ppm or mmu can be
off to take the analysis as valid confirmation. To get an idea which results
are fairly plausible, one can refer to the JOC guidelines, where m/z difference
within 0.003m/z (3 mmu) is considered valid for molecular masses below 1000
amu.\ [#acs_joc]_ And to RSC guidelines, where they accept difference within 5
ppm.\ [#rsc]_

Rules of a thumb
----------------
* if you are off by tens of mmu, its probably not your compound.
* if isotopic pattern fits and you are between 5 and 10 mmu, it may or may not
  be your compound.
* if you are within 5 mmu and the isotopic pattern fits, it may be your
  compound.
* if you are within ~2.5 mmu and isotopic pattern fits, it definitely may be
  your compound.
* If everything fits, it means your compound may be in the sample. It does not
  state anything about its purity.
* If you have higher m/z, it can still always be something else regardless of
  how precise the result look.


.. [#jan] j.zelenka@science.ru.nl

.. [#calc_webpage]
   you can calculate isotopic pattern in chemdraw. Or you can visit for
   instance https://www.chemcalc.org.

.. [#acs_jams]
   for a detailed description, refer to:
   https://publish.acs.org/publish/author_guidelines?coden=jamsef (author
   guidelines for Journal of the American Society for Mass Spetrometry)

.. [#acs_joc]
   https://publish.acs.org/publish/author_guidelines?coden=joceah#data_requirements

.. [#rsc]
   https://www.rsc.org/journals-books-databases/author-and-reviewer-hub/authors-information/prepare-and-format/experimental-reporting-requirements/

