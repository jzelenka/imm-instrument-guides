.. _solv_prep:

Solvent preparation
===================

Rules
-----
* Once solvents in the machine are low, they need to be refilled.
* Always use Milli-Q water and LC/MS solvent grade!
* When you're taking a bottle out of a cabinet, always check how many bottles
  are left. If there are less than **4 bottles**, new bottles have to be
  ordered.  For Huygens Wing4 MS cabinet, contact Helene.\ [#helene]_ For Mercator MS
  cabinet (M03.03.048/C1), contact Jan.\ [#jan]_
* Check if you should add 0.1% of formic acid or not (its labeled on the bottle
  by handwritten 0.1% Fa abbreviation). LC/MS grade formic acid is either in
  fridge (Wing 4 MS room), or in the solvent cabinet (Mercator). If you're
  opening the last bottle of the formic acid, contact Jan\ [#jan]_ or Helene\ [#helene]_.
* If unsure, do not worry to ask your MS Tutor, Jan\ [#jan]_ or Helene\ [#helene]_.

.. [#helene] Helene.Amatdjais-Groenen@ru.nl
.. [#jan] j.zelenka@science.ru.nl

