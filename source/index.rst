.. IMM Instrument Guides documentation master file, created by
   sphinx-quickstart on Thu Dec  2 20:04:48 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==================================
Welcome to IMM Instruments Guides!
==================================


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   instruments/index
   general/index
   specs_misc/index
   install/index
   special_instruments/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
