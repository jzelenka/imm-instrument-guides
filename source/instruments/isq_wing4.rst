.. _isq_wing_4:

ISQ mass spectrometer - Wing4 (MS)
==================================

.. toctree::
   :maxdepth: 2

   isq_wing4_water

If you are new to ISQ Wing 4 MS and need access to it, please read the
following guide.

How to operate the machine
--------------------------
I've made a video. Its not perfect, but most of the things are covered.
Bottlenecks of this video-guide are listed below. Once stable operation
procedure will be developed, fixed video will be recorded. `Operation
procedure`_ section covers the moust important parts.

Video (youtube)
^^^^^^^^^^^^^^^

..
    image, but not in a new widow
    .. image:: https://img.youtube.com/vi/M8Ol5ormuOg/0.jpg
            :target: https://www.youtube.com/watch?v=M8Ol5ormuOg

|youtube video|

.. |youtube video| raw:: html

   <a href="https://www.youtube.com/watch?v=M8Ol5ormuOg" target="_blank">
    <img src="https://img.youtube.com/vi/M8Ol5ormuOg/0.jpg"/>
   </a>

Notes (FAQ)
^^^^^^^^^^^
* PAY SPECIAL ATTENTION !! DO NOT SET BLANK AS TYPE:BLANK, keep the type as
  UNKNOWN (as shown in the video).
* PAY SPECIAL ATTENTION !! If you remove the sample tray from the autosampler,
  check that you put it back correctly, so it sits firmly on place.
* Sample tray is displayed in the video in the photo inset.
* Sample tray can be moved by hand when not injecting.
* Mass-range is predefined to full operational range
* keep attention to setting proper method - correct solvent and correct mode
  (positive/negative)
* Stay away from pollutants!!
* When using water follow the :ref:`water_samples` guide.


How to gain access
------------------
* Contact your MS tutor.
* Watch the video.
* Measure few times with the MS tutor.
* Request MS tutor to write me (Jan) and I will grant you access for the
  booking system (see :ref:`add_booking`)

Booking system
--------------
`Link to the booking system
<https://bookings.science.ru.nl/public/portal/index/domain/3/catid/25>`_

Sample preparation
------------------
Please refer to the :ref:`sample_preparation_guide`.

Operation procedure
-------------------
* Check if the solvent is there (> 300ml). If not, refill with pre-mix and
  check how many bottles are left (more in :ref:`solv_prep`).
* Insert your samples + blank.
* Copy the standard sample template (or [water template for water
  experiments](water.md)) to your folder and personalize it.
* Measure the samples.
* Evaluate the samples.
* Print / upload the outcome.
* Check that the machine is clean once you finish.
* Write a note into the logbook (its next to the machine, you will see notes

  from previous users there)

Network storage
---------------
Please refer to the :ref:`network_storage`.

When something goes wrong
-------------------------
DON'T PANIC! ;) It happens to all of us someday. Contact your MS tutor! If MS
tutor is not around contact some other MS tutor.

List of MS Tutors
-----------------
=========================== ==============
Name                        Group
=========================== ==============
None                        Eurostars
Calzari, M. (Matteo)        Boltje/Rutjes
Yvonne Bartels              Bonger
Hamstra, D.F.J. (Daan)      Bonger
Venrooij, K.R. (Kevin)      Bonger
Bruekers, J.P.J. (Jeroen)   Nolte
Gavriel, K. (Katerina)      Wilson
Knox, A.C. (Anna)           Huck
N.R.M. de Kler MSc (Noël)   Roithova
Moons, S.J. (Sam)           Synvenio
Poulladofonou, G. (Georgia) Velema
Shaghaghi, B. (Behrad)      Kouwer
Schenker, R.W. (Robin)      Löwik/Rutjes
Sinnige, W. (Wessel)        Löwik/Rutjes
=========================== ==============

Your group not in a list? **Become a MS tutor yourself!** (PhD preferred)


Questions/Misc
--------------
If something is wrong in this guide feel free to contact me.

TODO (planned improvements)
---------------------------
* SOP printout is still missing
* Explicitly show the type blank FAQ issue in the video !!



