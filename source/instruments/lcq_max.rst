LCQ Advantage Max mass spectrometer - Wing4 (MS-MS\ :sup:`n`)
=============================================================

If you are new to the LCQ Max and you need access to it or you want to refresh
your knowledge, please read the following guide.

How to operate the machine
--------------------------
Go to `Operation procedure`_ section.

Notes (FAQ)
^^^^^^^^^^^
* Stay away from pollutants (except special cases).

How to gain access
------------------
Contact Jan.\ [#jan]_


Booking system
--------------
`Link to the booking system
<https://bookings.science.ru.nl/public/portal/index/domain/3/catid/25>`_

Sample preparation
------------------
Please refer to the :ref:`sample_preparation_guide`.

Operation procedure
-------------------
* Inject some pure solvent to check for background contaminants.
* Inject your sample.
* Do advanced experiments.
* Wash the instrument.
* Inject pure solvent to check that instrument is clean.

Check `Detailed procedure`_ section for in-depth explanation.

Network storage
---------------
Please refer to the :ref:`network_storage`.

When something goes wrong
-------------------------
DON'T PANIC! ;) It happens to all of us someday. Contact Jan.\ [#jan]_

Detailed procedure
------------------
* Login with your science credentials.
* Start windowsrdp (icon on desktop)
* Check that the instrument is paused (icon #3 on image 1 is yellow and shows
  pause symbol)
* Check that ionization mode is set to one which you want (icon #6 on image 1)
* Open the Define Scan menu (icon #5 on image 1)
* Edit the scan parameters (image 2 and 3)
* Edit the source parameter (icon #1 on image 1, image 4)
* Optional: edit injection control (trap parameters, icon #2 on image 1, image
  5)
* Use blank solvent and spray turn machine on (icon #3 on image 1, after click
  it should look like green play button) - ideal state: you have signal, but no
  specific intense ions.
* Pause spraying (icon #3 on image 1, yellow pause), load your mixture.
* Spray your mixture (icon #3 on image 1, green play), adjust parameters to get
  what you need.
* Define recording (icon #4 on image 1).
* Record your measurement.
* Stop recording
* Stop spraying
* Flush the syringe.
* Flush the inlet
* Turn the spray on to check that everything is clean. (Important Step!)
* Turn the spray off.
* Remove solvent from syringe.
* Change settings to default, check that everything is in stable state.
* Copy your data to network share.
* Close windowsrdp, logout.

.. figure:: lcq_max_images/xcalib_whole.png
   :height: 246

   *Image 1: main window icons: 1) source parameters, 2) trap parameters, 3)
   on/pause switch, 4) recording, 5) scan parameters, 6) ionization mode
   switch.*

.. figure:: lcq_max_images/scan_params.png
   :height: 416

   *Image 2: Scan parameters, ms mode.*

.. figure:: lcq_max_images/scan_params_msn.png
   :height: 416

   *Image 3: Scan parameters, ms^n mode.*

.. figure:: lcq_max_images/source_params.png
   :height: 302

   *Image 4: ESI source parameters.*

.. figure:: lcq_max_images/trap_params.png
   :height: 334

   *Image 5: Trapping parameters.*




Questions/Misc
--------------
If something is wrong in this guide feel free to contact  Jan.\ [#jan]_

TODO (planned improvements)
---------------------------
* Make Prasopes compatible with the current setup.

.. [#jan] Jan Zelenka. I prefer using the @science mail -
   j.zelenka@science.ru.nl
