Instruments operation guides
============================

.. toctree::
   :maxdepth: 2

   isq_mercator
   isq_wing4
   lcq_fleet
   lcq_max
