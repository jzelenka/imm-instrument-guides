LCQ Fleet mass spectrometer - Wing4 (LC-MS)
===========================================

If you are new to the LCQ Fleet and you need access to it or you want to
refresh your knowledge, please read the following guide.

How to operate the machine
--------------------------
Go to `Operation procedure`_ section. In future, explanatory video will be made.

Notes (FAQ)
^^^^^^^^^^^
* Check that the sample tray is inserted properly.
* Check that the temperature of the oven is not far from the presetted one.
* Stay away from pollutants (except some special bypass cases).
* Wait for the first injection.

How to gain access
------------------
Contact Helene.\ [#helene]_


Booking system
--------------
`Link to the booking system
<https://bookings.science.ru.nl/public/portal/index/domain/3/catid/25>`_

Sample preparation
------------------
Please refer to the :ref:`sample_preparation_guide`.

Operation procedure
-------------------
* Check if there is enough of solvents (at least 400ml in all bottles).
* Put your samples into autosampler.
* Open your sequence, check if everything is okay.
* Turn on the pumps and PDA.
* Download method to the instrument
* Run your samples - start with a blank, end with a flush.
* Wait until you hear the first injection
* Add standby sequence after your sequence.
* Fill the logbook.

Network storage
---------------
Windows XP PC. No network storage connected.

When something goes wrong
-------------------------
DON'T PANIC! ;) It happens to all of us someday. Contact me or Helene.

Questions/Misc
--------------
If something is wrong in this guide feel free to contact me or Helene.\ [#helene]_

TODO (planned improvements)
---------------------------
* Fix the cooling fan of the column compartment.
* Reinstall to virtual-based environment.

.. [#helene] Helene.Amatdjais-Groenen@ru.nl
