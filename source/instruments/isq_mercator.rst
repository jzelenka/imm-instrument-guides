.. _isq_wing_1:

ISQ mass spectrometer - Mercator (LC-MS)
========================================

If you are new to ISQ Mercator LC-MS and need access to it, please read the
following guide.


How to operate the machine
--------------------------
There will be a video soon (hopefully). For now, please take the **traininig**
and follow the **operation procedure** section of this guide.

Video (youtube)
^^^^^^^^^^^^^^^
!TODO

Notes (FAQ)
^^^^^^^^^^^
* PAY SPECIAL ATTENTION !! DO NOT SET BLANK AS TYPE:BLANK, keep the type as
  UNKNOWN.
* PAY SPECIAL ATTENTION !! If you remove the sample tray from the autosampler,
  check that you put it back correctly, so it sits firmly on place.
* Sample tray can be moved by hand when not injecting.
* Mass-range is predefined to full operational range
* keep attention to setting a proper method
* Stay away from pollutants!!

How to gain access
------------------
* Contact your MS tutor.
* Read this webpage and the SOP
* Measure few times with the MS tutor.
* Request MS tutor to write me (Jan) and I will grant you access for the booking system (follow :ref:`add_booking`).

Booking system
--------------
`Link to the booking system
<https://bookings.science.ru.nl/public/portal/index/domain/3/catid/25>`_

Sample preparation
------------------
Please refer to the :ref:`sample_preparation_guide`.

Operation procedure
-------------------
* If you're the first one measuring on the day, switch on the UV and VIS lamps
  and wait ~10-15 minutes to let them stabilize.
* Check if the solvent is there (> 300ml). If not, refill with pre-mix and
  check how many bottles are left (more in :ref:`solv_prep`).
* Insert your samples + blank.
* Copy the standard sample template to your folder and personalize it.
* Measure the samples.
* Evaluate the samples.
* Print / upload the outcome.
* Check that the machine is clean once you finish.
* Write a note into the logbook (its next to the machine, you will see notes
  from previous users there).
* If you're the last one measuring on the day, switch off the UV lamps and check residual solvents and flow rate.

Network storage
---------------
Please refer to the :ref:`network_storage`.

When something goes wrong
-------------------------
DON'T PANIC! ;) It happens to all of us someday. Contact your MS tutor! If MS
tutor is not around contact some other MS tutor.

List of MS Tutors
-----------------
=========================== =================================
Name                        Group           
=========================== =================================
Kolk, M.R. van der (Marnix) Eurostars
Sondag, D. (Daan)           Boltje/Rutjes 
Meeusen, E. (Evy)           Boltje/Rutjes, not for training !
Ramos Cáceres, E. (Enebie)  Bonger
Gavriel, K. (Katerina)      Neumann/Wilson
N.R.M. de Kler MSc (Noël)   Roithova
Moons, S.J. (Sam)           Synvenio
Poulladofonou, G. (Georgia) Velema
Shaghaghi, B. (Behrad)      Kouwer
Schenker, R.W. (Robin)      Löwik/Rutjes
Sinnige, W. (Wessel)        Löwik/Rutjes
Zijlmans, L. (Luc)          Tagworks
=========================== =================================

Your group not in a list? **Become a MS tutor yourself!** (PhD preferred)


Questions/Misc
--------------
If something is wrong in this guide feel free to contact me.

If something is wrong with the instrument, contact me!

TODO (planned improvements)
---------------------------
* SOP is still missing
* Explicitly show the type blank FAQ issue in the video !!



