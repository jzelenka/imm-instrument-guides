Specification of the ISQ machines
==================================

Hardware of both machines is the same except the column.

ISQ™ EM Single Quadrupole Mass Spectrometer
-------------------------------------------
* P/N: `ISQEM-ESI
  <https://www.thermofisher.com/order/catalog/product/ISQEM-ESI>`_
* `Online manual - Feb/2019
  <https://assets.thermofisher.com/TFS-Assets/CMD/manuals/man-1r120591-0002-isq-ec-ms-man1r1205910002-en.pdf>`_
* :download:`Local manual - Feb/2019
  <manuals/man-1r120591-0002-isq-ec-ms-man1r1205910002-en.pdf>`
* Note: closely related ISQ™ EC has lower mass range (10-1250 instead of
  10-2000) and does not have APCI source option. [#]_

.. [#] :download:`ISQ™ EC/EM brochure
   <manuals/BR-72397-ISQ-EC-MS-BR72397-EN.pdf>`


UltiMate™ LPG-3400SD Standard Quaternary Pump
---------------------------------------------
* P/N: `5040.0031
  <https://www.thermofisher.com/order/catalog/product/5040.0031>`_
* `Online manual - v1.7, 2013 (up-to-date at 12-2021)
  <https://assets.thermofisher.com/TFS-Assets/CMD/manuals/MAN-4820-4001-LC-2G-Pumps-Operation.pdf>`_
* :download:`Local manual - v1.7, 2013 (up-to-date at 12-2021)
  <manuals/MAN-4820-4001-LC-2G-Pumps-Operation.pdf>`
* Maintenance kit P/N: `6040.1951
  <https://www.thermofisher.com/order/catalog/product/6040.1951>`_


UltiMate™ ACC-3000 T Thermostated Autosampler Column Compartment
----------------------------------------------------------------
* P/N: `5830.0020
  <https://www.thermofisher.com/order/catalog/product/5830.0020>`_
* `Online manual - v1.4, 2013 (probably up-to-date at 12-2021)
  <https://tools.thermofisher.com/content/sfs/manuals/64672-MAN-LC-ACC-3000-Operation-DOC4828-3050.pdf>`_
* :download:`Local manual - v1.4, 2013 (probably up-to-date at 12-2021)
  <manuals/64672-MAN-LC-ACC-3000-Operation-DOC4828-3050.pdf>`
* Parts:

  * Needle: 6820.2403
  * Needle capillary: 6830.2446
