Tubing specifications
=====================

Viper™ and nanoViper™ fitting systems
-------------------------------------
Special fittings used on Vanquish and UltiMate3000/ISQ systems.

* `Online brochure with color codes
  <https://assets.thermofisher.com/TFS-Assets/CMD/brochures/BR-72316-LC-Viper-nanoViper-Fitting-System-BR72316-EN.pdf>`_
* :download:`Local brochure with color codes - 04/2022
  <manuals/BR-72316-LC-Viper-nanoViper-Fitting-System-BR72316-EN.pdf>`
