LCQ calibration notes
=====================
Calibrations should be done by the Technician (me). If you think that the
calibration is wrong, please contact me.

Manual calibration
------------------
If an automated calibration fails, the instrument needs to be calibrated
manually first. Afterwards, one can proceed with the automated calibration.

To perform manual calibration:

* go to **Diagnostics->Diagnostics...->Calibration** tab. 
* At this point, you need two masses of which you know their precise peak
  position. These masses should be preferably on the opposite end of the m/z
  range (that means m/z~200 and m/z~1800). 
* Once you've located the peaks, fill in the values of the Expected/Observed
  Low/High mass.
* click Execute - you should see that the peaks moves to their appropriate
  locations.
* click Save

After these steps, you should be able to proceed with the automated
calibration.

.. image:: LCQ_calib_images/manual_calib.png
   :width: 500

*Diagnostics menu.*


Calibration mixture
-------------------
standard LCQ calibration mixture consists of\ [#calmix]_:

* caffeine m/z: **195.1**
* MRFA (Met-Arg-Phe-Ala, protonated) m/z: **524.3**
* Ultramark 1621\ [#ultramark]_ (mixture of fluorinated phosphazines) m/z: **1022.0, 1122.0, 1222.0, 1322.0, 1422.0, 1522.0, 1622.0, 1722.0, 1822.0, 1921.9**

.. [#calmix] https://www.thermofisher.com/nl/en/home/life-science/protein-biology/protein-biology-learning-center/protein-biology-resource-library/protein-biology-application-notes/mass-spec-calibration-solutions.html

.. [#ultramark] \a) \L. Jiang, M. Moini, *J. Am. Soc. Mass Spectrom.* **1992**, *3*, 842–846. DOI: `10.1016/1044-0305(92)80007-8 <http://dx.doi.org/10.1016/1044-0305(92)80007-8>`_ b) https://us.vwr.com/store/product/9887466/ultramark-1621-ultramark-mass-spec-standard





