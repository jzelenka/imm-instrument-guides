GC-2010 FID Gas chromatograph septum replacement
================================================
This guide of shorthand version of the official manual. For pictures and
detailed description, please go to the manual page 122. [#]_

.. [#] :download:`GC-2010 Gas Chromatograph INSTRUCTION MANUAL
   <files/GC-2010en.pdf>`

To replace the septum and reset the counter:

* Go to the [SYSTEM] -> Stop GC (PF menu)
* Remove the autosampler (pull it up).
* Wait for "GC is ready for maintenance" message.
* Open the top lid
* Both oven and injection port should be cooled to 50 °C (or below) and inlet
  pressure should be 0 kPa.
* Unscrew the septum nut.
* Replace the septum. Do not loose the metal needle guide which is in the
  septum! Do not touch the new septum by hand! (use tweezers)
* Tighten the new septum as far as you can (handtight). 
* Return a semicircle (180°).
* Close the top lid.
* select [Anal.] from PF menu. GC should flow carrier gas for 5 miutes and
  return to operating temperature.
* Go to [DIAG] -> Analysis Counter, go to the analysis counter, click reset.
* Test the system sanity by running a sample.
* If needed do a blank run(s) to remove pollution from the replacement.

