PDF export on Windows XP virtual machines
=========================================

For printing into PDF on WXP instruments, following software has been
installed:

* CutePDF™ Writer version 3.2\ [#cutepdf]_
* Ghostscript 9.0.6\ [#gs]_

.. [#cutepdf] https://download.cutepdf.com/public/cutewriter32.exe, copy
   :download:`here<files/cutewriter32.exe>`

.. [#gs] the links to converter file on cutepdf website leads to a broken exe
   file, the working one is in the bundle here:
   http://www.cutepdf.com/download/CuteWriter.zip, copy
   :download:`here<files/converter.exe>`
