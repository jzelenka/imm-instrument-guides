Installing Finnigan Surveyor Autosampler with an LC pump
========================================================

This guide covers some of the less intuitive steps in the installation of the
surveyor autosampler.

Ethernet Network topology
-------------------------
In the PC, 2 networks cards must be used for connecting of the LC system and
LCQ.

* 1\ :sup:`st` card is connected directly to the LCQ
* 2\ :sup:`nd` card is connected to a switch which is connected to the LC pump
  and to the autosampler.

Ethernet Network configuration
------------------------------

* The 1\ :sup:`st` card (LCQ) IP address is set to 10.0.0.101, subnet mask is
  255.255.255.0.
* The 2\ :sup:`nd` card (LC system) IP address is set to 172.16.0.101, subnet
  mask is 255.255.0.0.

5-way cable connection
----------------------
The 5-way cable connection is connected in accordance to the manual, that means
that blue LC pump plug is counter-intuitively plugged to the LC pump although
the port on the LC pump is labeled by a yellow MS pump label. Plug to the "MS
pump" and to the "Detector" are left unplugged.

Surveyor autosampler instrument configuration
---------------------------------------------
It is crucial to properly configure signal polarity in the Surveyor Autosampler
Configuration. That means: all checkboxes should be unchecked (including Pump
read active high and Injection hold release active high). If not, after
submitting a queue, autosampler will be in "Running" state with a notification
"Waiting for pump" and pump will be in "Waiting for contact closure" state.

Notes
-----
Initial version. Links to proper manuals and photos will be added in future.
