Unlocking LCQ Advantage MAX MS\ :sup:`n`
========================================

Description
-----------
Unlike other LCQs which I've met, on LCQ Advantage MAX it is not possible to
perform MS\ :sup:`n` experiments with a default installation. Moreover, neither
"Activation Q" nor "Activation time (msec)" can be set. However, all these
features can be activated by "easy upgrade to MS\ :sup:`n` performace."\
[#brochure]_ I am unsure, if there are some hardware changes, but MS\ :sup:`n`
gets unlocked if a correct license key is present in the Instrument
Configuration.

.. [#brochure] :download:`Finnigan™ LCQ™ Advantage MAX Product Specifications
   <files/LCQ-Advantage-Max-Specifications.pdf>`

Procedure
---------
Unfortunately, if the correct key is provided through the Instrument
Configuration utility, it is not accepted and an error message pops-up. To
Address this issue, regedit value must be copied from a working installation.

Steps:

* Copy regedit value from a licensed installation. On licensed installation
  type *WinKey + r*, type regedit. Then go to
  HKEY_LOCAL_MACHINE/SOFTWARE/Finnigan/XLic/LCQ and copy the "License" value.
* Paste the license value to a non-licensed installation onto the same place in
  regedit.
* New installation should be now working as a licensed one.

Disclaimer
----------
As we own the license, this procedure only helps us to practice our rights. No
other party rights have been violated. This guide does not in any way encourage
to violate any legal rights.

Notes
-----
MAC address of the instrument was the same on both machines Instrument
Configuration. The same applies for the instrument name, for
HKEY_LOCAL_MACHINE/SOFTWARE/Finnigan/XLic/LCQ/License2 regedit value and for a
license key of the XCalibur installation itself. It has not been tested what
happens if these values differ.
