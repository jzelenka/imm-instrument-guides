Installation of the ISQ/chromeleon
==================================

Hardware of both machines is the same except the column.

Network configuration
---------------------
Network connection from PC to ISQ instrument has the following IPv4
configuration:

* IP Address: 192.168.1.1
* Net mask: 255.255.255.0

Services fails to start
-----------------------
After some adjustments, Chromeleon services kept failing to start. The
underlying cause was that the instruments were delivered with HDDs instead of
SSDs Because of that, starting of Net.TCP Port Sharing Service took longer than
what was the default timeout. Extending the service start timeout by changing
regedit as described on serverfault\ [#]_ fixed the issue.

Steps were following:

* Ctrl + R --> regedit
* go to HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control
* create new entry - right click -> new -> DWORD
* set new DWORD name to ServicesPipeTimeout and value to 1 200 000 (decimal)

As there were still some issues, I've extended Chromeleon services delay
between resets from 0 to 5 minutes. Afterwards, the ISQ was running smoothly


.. [#] https://serverfault.com/questions/622432/how-do-i-increase-windows-service-startup-timeout
